package com.puseletsomaraba.inspire;


import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.concurrent.ThreadLocalRandom;


/**
 * A simple {@link Fragment} subclass.
 */
public class QuoteFragment extends Fragment {


    public QuoteFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View quoteView = inflater.inflate(R.layout.fragment_quote, container, false);
        TextView quoteText = quoteView.findViewById(R.id.quoteText);
        TextView byAuthor = quoteView.findViewById(R.id.byAuthor);
        CardView cardView=quoteView.findViewById(R.id.cardView);

        String quote  = getArguments().getString("quote");
        String author=getArguments().getString("author");
        int colors[]=new int[]{R.color.blue_500,R.color.red_500,R.color.amber_500,R.color.purple_500,
                R.color.pink_500,R.color.lime_500,R.color.red_A100,R.color.cyan_500,
                R.color.blue_900,R.color.green_500,R.color.green_100,R.color.indigo_500};

        quoteText.setText(quote);
        byAuthor.setText("-"+author);

        cardView.setBackgroundResource(getRandom(colors));

        return quoteView; }

    public static final  QuoteFragment newInstance(String quote,String author){

        QuoteFragment fragment = new QuoteFragment();
        Bundle bundle = new Bundle();
        bundle.putString("quote",quote);
        bundle.putString("author",author);
        fragment.setArguments(bundle);

        return fragment;

    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    int getRandom(int[] colorArray){

        int color;
        int quotesArrayLen = colorArray.length;

        int randomNum = ThreadLocalRandom.current().nextInt(quotesArrayLen);
        color=colorArray[randomNum];

        return color;

    }

}

