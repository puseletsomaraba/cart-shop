package com.puseletsomaraba.inspire.data;

import com.puseletsomaraba.inspire.model.Quote;

import java.util.ArrayList;

public interface QuoteListAsyncResponse {
    void processFinish(ArrayList<Quote>quotes);
}
